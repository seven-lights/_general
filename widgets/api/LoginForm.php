<?php
/**
 * Project: blog
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\widgets\api;


use general\ext\api\auth\AuthUrlCreator;
use yii\base\Widget;

class LoginForm extends Widget {
	public $service;
	public $view;
	public $retUrl;
	public $class = 'login';
	public $id = '';
	public function run() {
		return $this->render('LoginForm', [
			'url' => self::proccessUrl($this->service, $this->view, $this->retUrl),
			'class' => $this->class,
			'id' => $this->id,
		]);
	}
	public static function proccessUrl($service, $view, $retUrl) {
		return AuthUrlCreator::userAuthenticate($service, $view, $retUrl);
	}
}
