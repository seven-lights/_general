<?php
/**
 * Project: blog
 * User: Sergey Bessonov
 * E-mail: sergebessonov@gmail.com
 */

namespace general\widgets;


use yii\base\Widget;
use yii\helpers\Html;

class SkyTags extends Widget {
	public $items; // массив ключ = тег, значение = частота использования тега
	public $class; // дописывается градация от 0 до 17
	public $link;
	public $minRank = 0;
	public $attribute;


	public function run() {
		$coef = 17 / max($this->items);
		$res = '';
		foreach($this->items as $key => $tagVal) {
			if(intval($tagVal * $coef) > $minRank) {
				$res .= Html::a($key,
					[$this->link, $this->attribute => $key],
					['class' => $this->class . intval($tagVal * $coef)]
				);
			}
		}
		return $res;
	}
} 