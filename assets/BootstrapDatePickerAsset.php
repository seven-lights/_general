<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace general\assets;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the [jquery javascript library](http://jquery.com/)
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BootstrapDatePickerAsset extends AssetBundle
{
    public $sourcePath = '@general/distr/plugins/datepicker';
	public $css = [
		'datepicker3.css',
	];
    public $js = [
        'bootstrap-datepicker.js',
    ];
	public $depends = [
		'yii\bootstrap\BootstrapPluginAsset',
	];
}
