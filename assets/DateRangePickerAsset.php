<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace general\assets;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the [jquery javascript library](http://jquery.com/)
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DateRangePickerAsset extends AssetBundle
{
    public $sourcePath = '@general/distr/plugins/daterangepicker';
	public $css = [
		'daterangepicker-bs3.css',
	];
    public $js = [
        'daterangepicker.js',
    ];
	public $depends = [
		'yii\web\JqueryAsset',
	];
}
