<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext;


use yii\base\Object;

class Helper extends Object{
	public static function translit ($str) {
		$translit_array = array (
			'а'=>'a',   'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'ye', 'ё'=>'yo', 'ж'=>'zh', 'з'=>'z',
			'и'=>'i',   'й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n',  'о'=>'o',  'п'=>'p',
			'р'=>'r',   'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h',  'ц'=>'c',  'ч'=>'ch', 'ш'=>'sh',
			'щ'=>'sch', 'ъ'=>'',  'ы'=>'y', 'ь'=>'',  'э'=>'e', 'ю'=>'yu', 'я'=>'ya',
			'А'=>'A',   'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D', 'Е'=>'YE', 'Ё'=>'YO', 'Ж'=>'ZH', 'З'=>'Z',
			'И'=>'I',   'Й'=>'Y', 'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N',  'О'=>'O',  'П'=>'P',
			'Р'=>'R',   'С'=>'S', 'Т'=>'T', 'У'=>'U', 'Ф'=>'F', 'Х'=>'H',  'Ц'=>'C',  'Ч'=>'CH', 'Ш'=>'SH',
			'Щ'=>'SCH', 'Ъ'=>'',  'Ы'=>'Y', 'Ь'=>'',  'Э'=>'E', 'Ю'=>'YU', 'Я'=>'YA', ' '=>'-'
		);
		return preg_replace ('%[^\w-]*%', '', preg_replace ('%[-]{2,}%', '-', strtr ( trim( $str ), $translit_array ) ) );
	}
	public static function cutter($content, $length = 255, $encoding = 'UTF-8'){
		$text = strip_tags($content);
		if(mb_strlen($text, $encoding) > $length) {
			$text = mb_substr($text, 0, $length, $encoding);
			$pos = mb_strrpos($text, ' ', null, $encoding);
			if($pos) {
				return mb_substr($text, 0, $pos, $encoding);
			}
			return $text;
		}
		return $text;
	}
	public static function ucfirst_utf8($stri){
		if($stri{0}>="\xc3")
			return (($stri{1}>="\xa0")?
				($stri{0}.chr(ord($stri{1})-32)):
				($stri{0}.$stri{1})).substr($stri,2);
		else return ucfirst($stri);
	}
} 