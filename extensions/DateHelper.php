<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15.02.2016
 * Time: 12:04
 */

namespace general\ext;


use yii\base\Object;

class DateHelper extends Object
{
    private static $month_min = [
        'ru' => [
            1 => 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек',
        ]
    ];
    private static $month = [
        'ru' => [
            1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь',
        ]
    ];
    public static function getMonthMin($num, $lang = 'ru') {
        return self::$month_min[ $lang ][ (int)$num ];
    }
}