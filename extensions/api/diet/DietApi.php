<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\diet;


use general\ext\api\BaseApi;

class DietApi extends BaseApi {
	private static $service = 'diet';
	public static function categoryOfArticleIndex($parent_id = null, $sort = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if(is_null($parent_id)) {
			return self::execute(self::$service, 'category-of-article/index', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'sort' => $sort,
			]);
		} else {
			return self::execute(self::$service, 'category-of-article/index', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'parent_id' => (int)$parent_id,
				'sort' => $sort,
			]);
		}
	}
	public static function categoryOfArticleCreate(array $CategoryOfArticle) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($CategoryOfArticle as $k => $v) {
			$post['CategoryOfArticle[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'category-of-article/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
			],
			$post
		);
	}
	public static function categoryOfArticleUpdate($id, array $CategoryOfArticle = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		if($CategoryOfArticle === []) {
			return self::execute(self::$service,
				'category-of-article/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				]
			);
		} else {
			$post = [];
			foreach ($CategoryOfArticle as $k => $v) {
				$post['CategoryOfArticle[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'category-of-article/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function categoryOfArticleDelete($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'category-of-article/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
	}
	public static function categoryOfArticleMaxNumber($parent_id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'category-of-article/max-number', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'parent_id' => $parent_id,
		]);
	}

	public static function categoryOfProductIndex($parent_id = null, $sort = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		if(is_null($parent_id)) {
			return self::execute(self::$service, 'category-of-product/index', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'sort' => $sort,
			]);
		} else {
			return self::execute(self::$service, 'category-of-product/index', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
				'parent_id' => (int)$parent_id,
				'sort' => $sort,
			]);
		}
	}
	public static function categoryOfProductCreate(array $CategoryOfProduct) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($CategoryOfProduct as $k => $v) {
			$post['CategoryOfProduct[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'category-of-product/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
			],
			$post
		);
	}
	public static function categoryOfProductUpdate($id, array $CategoryOfProduct = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		if($CategoryOfProduct === []) {
			return self::execute(self::$service,
				'category-of-product/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				]
			);
		} else {
			$post = [];
			foreach ($CategoryOfProduct as $k => $v) {
				$post['CategoryOfProduct[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'category-of-product/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function categoryOfProductDelete($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'category-of-product/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
	}
	public static function categoryOfProductMaxNumber($parent_id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'category-of-product/max-number', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'parent_id' => $parent_id,
		]);
	}

	public static function productIndex($get = [], $sort = '') {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'product/index',
			array_merge(
				[
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'sort' => $sort,
				],
				$get
			)
		);
	}
	public static function productCreate(array $Product) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($Product as $k => $v) {
			$post['Product[' . $k . ']'] = $v;
		}

		return self::execute(self::$service,
			'product/create', [
				'access_key' => $access_key,
				'service' => \Yii::$app->id,
			],
			$post
		);
	}
	public static function productUpdate($id, array $Product = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		if($Product === []) {
			return self::execute(self::$service,
				'product/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				]
			);
		} else {
			$post = [];
			foreach ($Product as $k => $v) {
				$post['Product[' . $k . ']'] = $v;
			}

			return self::execute(self::$service,
				'product/update', [
					'access_key' => $access_key,
					'service' => \Yii::$app->id,
					'id' => $id,
				],
				$post
			);
		}
	}
	public static function productDelete($id) {
		if(!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'product/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'id' => $id,
		]);
	}
}