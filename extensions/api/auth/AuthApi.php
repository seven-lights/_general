<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\api\auth;


use general\ext\api\BaseApi;

class AuthApi extends BaseApi
{
	private static $service = 'auth';

	public static function userSearch($search, $page = 0) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($search as $k => $v) {
			$post['search[' . $k . ']'] = $v;
		}

		$ans = self::execute(self::$service, 'user/search', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'page' => $page,
		], $post);
		return $ans['result'] == 'error' ? false : $ans;
	}

	public static function userGetServiceAuthKey($mark)
	{
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		$ans = self::execute(self::$service, 'user/get-service-auth-key', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'key' => $mark,
		]);
		return $ans['result'] == 'error' ? false : $ans;
	}

	public static function userChangePassword($user_id, $oldPassword, $password)
	{
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}
		return self::execute(self::$service, 'user/change-password', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'user_id' => $user_id,
			'oldPassword' => $oldPassword,
			'password' => $password,
		]);
	}

	public static function userSignup($user) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		$post = [];
		foreach ($user as $k => $v) {
			$post['user[' . $k . ']'] = $v;
		}

		return self::execute(self::$service, 'user/signup', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
		], $post);
	}

	public static function userDelete($user_id) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

		return self::execute(self::$service, 'user/delete', [
			'access_key' => $access_key,
			'service' => \Yii::$app->id,
			'user_id' => $user_id
		]);
	}
	public static function userUpdate($id, $user = []) {
		if (!$access_key = self::getReceivedAccessKey(self::$service)) {
			return false;
		}

        if($user === []) {
            return self::execute(self::$service, 'user/update', [
                'access_key' => $access_key,
                'service' => \Yii::$app->id,
                'user_id' => $id,
            ]);
        } else {
            $post = [];
            foreach ($user as $k => $v) {
                $post['user[' . $k . ']'] = $v;
            }

            return self::execute(self::$service, 'user/update', [
                'access_key' => $access_key,
                'service' => \Yii::$app->id,
                'user_id' => $id,
            ], $post);
        }
	}
}