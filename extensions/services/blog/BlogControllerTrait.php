<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace general\ext\services\blog;


use general\ext\api\blog\BlogApi;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

trait BlogControllerTrait {
	public function actionList($page = 0, $tag = null) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$data = BlogApi::postList(static::$domain, $page, $tag);
		if($data['result'] == 'success') {
			$dataProvider = new ArrayDataProvider([
				'allModels' => $data['posts'],
				'pagination' => [
					'page' => $page,
					'totalCount' => $data['totalCount'],
					'pageSize' => 30,
				],
			]);
			return $this->render('list', [
				'dataProvider' => $dataProvider,
				'tag' => $tag,
			]);
		}
		throw new NotFoundHttpException();
	}
	public function actionView($id) {
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$data = BlogApi::postView(static::$domain, $id);
		if($data['result'] == 'success') {
			return $this->render('view', [
				'data' => $data['post'],
			]);
		}
		throw new NotFoundHttpException();
	}
}