<?php
/**
 * Project: Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

return [
	'secrets' => [
		'auth' => 'secret',
		'personal' => 'secret',
		'diet' => 'secret',
		'blog' => 'secret',
		'admin' => 'secret',
		'passport' => 'secret',
		'page' => 'secret',
		'billiard' => 'secret',
		'sport' => 'secret',

		'test' => 'secret',
	],
	'expires_on' => 86400,
	'origins' => [
		'auth' => 'http://auth.seven.loc',
		'blog' => 'http://blog.loc',
		'diet' => 'http://diet.loc',
		'passport' => 'http://passport.seven.loc',
		'page' => 'http://page.seven.loc',
		'billiard' => 'http://billiard.loc',
		'sport' => 'http://sport.loc',

		'test' => 'http://test.loc',
	],
	'urls' => [
		'auth' => 'auth.seven.loc/api/',
		'blog' => 'blog.loc/api/',
		'diet' => 'diet.loc/api/',
		'passport' => 'passport.seven.loc/api/',
		'page' => 'page.seven.loc/api/',
		'billiard' => 'billiard.loc/api/',
		'sport' => 'sport.loc/api/',

		'test' => 'test.loc/api/',
	],
	'loginUrls' => [
		'blog' => 'blog.loc/site/login-with-auth-key',
		'admin' => 'admin.loc/site/login-with-auth-key',
		'diet' => 'diet.loc/site/login-with-auth-key',
		'billiard' => 'billiard.loc/site/login-with-auth-key',
		'sport' => 'sport.loc/site/login-with-auth-key',

		'test' => 'test.loc/site/login-with-auth-key',
	],
];